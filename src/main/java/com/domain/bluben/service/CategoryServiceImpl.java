package com.domain.bluben.service;

import com.domain.bluben.dto.CategoryDto;
import com.domain.bluben.model.Category;
import com.domain.bluben.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    // UUID s1 = categoryRepository.softDelete(1);

    @Override
    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category update(UUID id, Category category) {
        return categoryRepository.save(category);
    }

    @Override
    // @PostConstruct
    public void delete(UUID id) {
        categoryRepository.softDelete(id);
    }

    @Override
    public Object findAll(String name, Integer pageSize, Integer page, String sort) {

        var value1 = sort.equals("asc") ? categoryRepository.searchName(name, pageSize, page) : sort.equals("desc") ?
                categoryRepository.searchNameDesc(name, pageSize, page) : categoryRepository.searchName(name, pageSize, page);

        double totalData = categoryRepository.findAll().size();
        Map<String, Object> response = new HashMap<>();
        response.put("result", value1);
        response.put("Count", name == null ? (int) totalData : value1.size());
        response.put("Total_page", (int) Math.ceil(name == null ? totalData : (double) value1.size() / pageSize));
        return response;
    }

    @Override
    public Category findById(UUID id) {
        Optional<Category> result = categoryRepository.findById(id);
        return result.orElse(null);
    }

    @Override
    public CategoryDto mapToDto(Category category) {
        return null;
    }

    @Override
    public Category mapToEntity(CategoryDto categoryDto) {
        return null;
    }
}
