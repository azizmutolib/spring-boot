package com.domain.bluben.service;

import com.domain.bluben.dto.LoginDto;
import com.domain.bluben.dto.RegisterDto;
import com.domain.bluben.model.Account;
import com.domain.bluben.model.Karyawan;
import com.domain.bluben.model.Role;

import java.util.List;
import java.util.UUID;

public interface AuthService {

    Account register(Account account, Karyawan karyawan);
    Object login(LoginDto loginDto);
    Role findRole(UUID id);
    RegisterDto maptoDto(Account account);
    Account mapToEntity(RegisterDto registerDto);
    LoginDto mapToDto(Account account);
    Account maptoEntity(LoginDto loginDto);
   Account findone(UUID account);



}
