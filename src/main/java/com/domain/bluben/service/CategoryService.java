package com.domain.bluben.service;

import com.domain.bluben.dto.CategoryDto;
import com.domain.bluben.model.Category;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public interface CategoryService {

    Category create(Category category);

    Category update(UUID id, Category category);

    void delete(UUID id);

    Object findAll(String name, Integer pageSize, Integer page,String sort);

    Object findById(UUID id);

    CategoryDto mapToDto(Category category);

    Category mapToEntity(CategoryDto categoryDto);
}
