package com.domain.bluben.service;

import com.domain.bluben.config.JwtTokenUtil;
import com.domain.bluben.dto.LoginDto;
import com.domain.bluben.dto.RegisterDto;
import com.domain.bluben.model.Account;
import com.domain.bluben.model.JwtResponse;
import com.domain.bluben.model.Karyawan;
import com.domain.bluben.model.Role;
import com.domain.bluben.repository.AuthRepository;
import com.domain.bluben.repository.KaryawanRepository;
import com.domain.bluben.repository.RoleRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.juli.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

@Service
@Transactional
public class AuthServiceimpl implements AuthService {

    @Autowired
    AuthRepository authRepository;

    @Autowired
    KaryawanRepository karyawanRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Override
    public Account register(Account account, Karyawan karyawan) {
        karyawanRepository.save(karyawan);
        return authRepository.save(account);
    }

    @Override
    public Object login(LoginDto loginDto) {
        try {
            Account user = authRepository.searchemail(loginDto.email());
            if (user.getEmail().equals("yssosga1@gmail.com")){

            }
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (user == null) {
                return "User not founs";
            } else if (loginDto.email().equalsIgnoreCase(user.getEmail())
                    && encoder.matches(loginDto.password(), user.getPassword())) {
                final String token=jwtTokenUtil.generateToken(user);
                return new JwtResponse(token);
            } else {
                return "Password incorrect";
            }
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    @Override
    public Role findRole(UUID id) {
        Optional<Role> result = roleRepository.findById(id);
        return result.orElse(null);
    }

    ObjectMapper mapper = new ObjectMapper();

    @Override
    public RegisterDto maptoDto(Account account) {
        return mapper.convertValue(account, RegisterDto.class);
    }

    @Override
    public Account mapToEntity(RegisterDto registerDto) {
        return mapper.convertValue(registerDto, Account.class);
    }

    @Override
    public LoginDto mapToDto(Account account) {
        return mapper.convertValue(account, LoginDto.class);
    }

    @Override
    public Account maptoEntity(LoginDto loginDto) {
        return mapper.convertValue(loginDto, Account.class);
    }

    @Override
    public Account findone(UUID account) {
        Optional<Account> ada= authRepository.findById(account);
        return ada.orElse(null);
    }
}
