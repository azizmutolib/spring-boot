package com.domain.bluben.helper;

import java.util.Random;

public class SaltPassword {

    public static String getSaltString() {
        String saltchar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890qwertyuiopasdfghjklzxcvbnm";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 21) { // length of the random string.
            int index = (int) (rnd.nextFloat() * saltchar.length());
            salt.append(saltchar.charAt(index));
        }
        return salt.toString();

    }
}
