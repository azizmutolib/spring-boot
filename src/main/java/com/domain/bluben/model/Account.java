package com.domain.bluben.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.UUID;


@Getter
@Setter
@Table(name = "account")
//@SQLDelete(sql = "UPDATE category SET deleted_at=true WHERE id=?")
@Where(clause = "account0_.deleted_at = false")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Account {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    public UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(unique = true)
    @NotEmpty(message = "Email is required")
    private String email;

    @Column(nullable = false)
    @NotEmpty(message = "Password required")
    private  String password;

    private  String salt;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column()
    public Boolean deleted_at;

    @ManyToOne(targetEntity = Role.class,fetch = FetchType.EAGER)
    private Role role;
}
