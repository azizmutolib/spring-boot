package com.domain.bluben.controller;

import com.domain.bluben.dto.LoginDto;
import com.domain.bluben.dto.RegisterDto;
import com.domain.bluben.helper.ResponseHandler;
import com.domain.bluben.helper.SaltPassword;
import com.domain.bluben.model.Account;
import com.domain.bluben.model.Karyawan;
import com.domain.bluben.model.Role;
import com.domain.bluben.repository.AuthRepository;
import com.domain.bluben.repository.RoleRepository;
import com.domain.bluben.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;
import java.util.UUID;

@Controller
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @Autowired
    RoleRepository roleRepository;

    @PostMapping("/register")
    public Object register(@RequestBody RegisterDto request) {
        try {
            Role role = roleRepository.SearchNameRole(request.role());
            Account ac = new Account();
            Karyawan kr = new Karyawan();
            ac.setSalt(SaltPassword.getSaltString());
            ac.setDeleted_at(false);
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
            String hashedPassword = bCryptPasswordEncoder.encode(request.password());
            ac.setPassword(hashedPassword);
            ac.setEmail(request.email());
            ac.setRole(role);
            ac.setName(request.name());
            kr.setName(request.name());
            kr.setAccount(ac);
            Account result = authService.register(ac, kr);
            return ResponseHandler.generateResponse("Successfully add data!", HttpStatus.OK, result);
        } catch (Exception e) {
            if (e.getMessage().equals("could not execute statement; SQL [n/a]; constraint [uk_q0uja26qgu1atulenwup9rxyr]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement")){
                return ResponseHandler.generateResponse("email already exists!", HttpStatus.BAD_REQUEST, null);
            }
                return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, null);
        }
    }

    @PostMapping("/login")
    public Object login(@RequestBody LoginDto request) {
        try {
            var result = authService.login(request);
            if (request.email() == null) {
                result = null;
                return ResponseHandler.generateResponse("Email cannot be null!", HttpStatus.BAD_REQUEST, result);
            }
            if (result.equals("User not found")) {
                result = null;
                return ResponseHandler.generateResponse("Email incorrect!", HttpStatus.BAD_REQUEST, result);
            } else if (result.equals("rawPassword cannot be null")) {
                result = null;
                return ResponseHandler.generateResponse("Password cannot be null", HttpStatus.BAD_REQUEST, result);
            } else if (result.equals("Password incorrect")) {
                result = null;
                return ResponseHandler.generateResponse("Password incorrect!", HttpStatus.BAD_REQUEST, result);
            } else {
                return ResponseHandler.generateResponse("Success!", HttpStatus.OK, result);
            }
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, null);

        }
    }

    @GetMapping("/{id}")
    public Object findaccount(@PathVariable UUID id){
        return authService.findone(id);
    }
}
