package com.domain.bluben.controller;

import com.domain.bluben.dto.CategoryDto;
import com.domain.bluben.helper.ResponseHandler;
import com.domain.bluben.model.Category;
import com.domain.bluben.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping("/create")
    public Object create(@RequestBody CategoryDto request) {
        try {
            Category cat = new Category();
            cat.setName(request.name());
            cat.setDeleted_at(false);
            cat.setId(UUID.randomUUID());
            Category result = categoryService.create(cat);
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.CREATED, result);
        } catch (Exception e) {
            return "Category is already in use";
        }

    }

    @GetMapping("")
    public Object findAll(@RequestParam(required = false) String name, @RequestParam(required = false) Integer page,
                          @RequestParam(required = false) Integer pageSize, @RequestParam(required = false) String sort) {
        try {
            if (page == null) {
                page = 1;
            }
            if (pageSize == null) {
                pageSize = 2;
            }if (sort==null){
                sort="asc";
            }

            page -= 1;
            var result = categoryService.findAll(name, pageSize, page,sort);
            return ResponseHandler.generateResponse("Success", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse("gagal", HttpStatus.BAD_REQUEST, null);
        }
    }

    @GetMapping("/detail/{id}")
    public Object findById(@PathVariable UUID id) {
        try {
            var result = (Category) categoryService.findById(id);
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, null);
        }
    }

    @PutMapping("/update/{id}")
    public Object update(@PathVariable UUID id, @RequestBody CategoryDto request) {
        Category a = (Category) categoryService.findById(id);
        if (a != null) {
            a.setName(request.name());
            try {
                return categoryService.update(id, a);
            } catch (Exception e) {
                return "Category name is already in use";
            }

        } else {
            return ResponseHandler.generateResponse("Data not found!", HttpStatus.NOT_FOUND, null);
        }

    }

    @DeleteMapping("/delete/{id}")
    public Object delete(@PathVariable UUID id) {
        Category a = (Category) categoryService.findById(id);
        if (a != null) {
            categoryService.delete(id);
            return ResponseHandler.generateResponse("Successfully deleted data!", HttpStatus.OK, a);
        }
        return ResponseHandler.generateResponse("Data not found!", HttpStatus.NOT_FOUND, null);
    }

}
