package com.domain.bluben.repository;

import com.domain.bluben.model.Account;
import com.domain.bluben.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    @Query(value = "Select * from Role as e  where e.name =:name", nativeQuery = true)
    Role SearchNameRole(@Param("name") String name);
}
