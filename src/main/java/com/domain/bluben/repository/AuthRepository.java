package com.domain.bluben.repository;

import com.domain.bluben.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface AuthRepository extends JpaRepository<Account , UUID> {
    @Query(value = "Select * from Account as e   where e.email =:email", nativeQuery = true)
    Account searchemail(@Param("email") String email);

}
