package com.domain.bluben.repository;

import com.domain.bluben.model.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface KaryawanRepository extends JpaRepository<Karyawan, UUID> {
}
