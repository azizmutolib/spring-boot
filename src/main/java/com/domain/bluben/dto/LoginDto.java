package com.domain.bluben.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public record LoginDto( String email, String password ) {

}
