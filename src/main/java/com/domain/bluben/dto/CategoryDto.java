package com.domain.bluben.dto;

public record CategoryDto(String name) {
}
