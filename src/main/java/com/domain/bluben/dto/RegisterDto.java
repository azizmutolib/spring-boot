package com.domain.bluben.dto;

import java.util.UUID;

public record RegisterDto(String name, String email, String password,String role) {
}
